import express from 'express'
import RoutesV1 from './routes/v1/index.js'
import cors from 'cors'
const app = express()

const getApp = async () => {
  app.use(
    cors({
      origin: '*'
    })
  )
  app.use('/v1', RoutesV1)
  return app
}

export default getApp
