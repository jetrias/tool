import express from 'express'
import axios from 'axios'
import {} from 'dotenv/config'
const router = express.Router()

router.get('/data', async function (req, res) {
  const dataTosend = []
  let lines = []
  const errArr = []
  const config = {
    headers: {
      authorization: 'Bearer aSuperSecretKey',
      accept: 'application/json'
    }
  }
  const response = await axios
    .get(`${process.env.SERVERDIR}/v1/secret/files`, config)
    .catch((e) => {
      throw new Error(e)
    })
  if (response.status === 200) {
    const files = response.data.files
    for (const f of files) {
      const responseFile = await axios
        .get(`${process.env.SERVERDIR}/v1/secret/file/${f}`, config)
        .catch((e) => {
          errArr.push({ file: f, error: e.response.data.message })
        })
      if (responseFile) {
        if (responseFile.status === 200) {
          const result = responseFile.data.split(/\r?\n/)
          result.shift()
          if (result.length > 1) {
            for (let i = 0; i < result.length; i++) {
              const dat = result[i].split(',')
              if (dat.length === 4 && !isNaN(dat[2]) && dat[3].length === 32) {
                lines.push({
                  text: dat[1],
                  number: Number(dat[2]),
                  hex: dat[3]
                })
              }
            }
            dataTosend.push({
              file: f,
              lines
            })
            lines = []
          }
        }
      }
    }
  }
  const dat = []
  dataTosend.forEach(d => {
      if (typeof d.lines!=='undefined' && d.lines.length !== 0) {
        dat.push(d)
      }
  })
  return res.status(200).json({ data: dat, filesWithErrors: errArr })
})

router.get('/list', async function (req, res) {
  const config = {
    headers: {
      authorization: 'Bearer aSuperSecretKey',
      accept: 'application/json'
    }
  }
  const response = await axios
    .get(`${process.env.SERVERDIR}/v1/secret/files`, config)
    .catch((e) => {
      throw new Error(e)
    })
  if (response.status === 200) {
    const files = response.data.files
    res.status(200).json({ files })
  }
})

export default router
