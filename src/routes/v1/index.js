import express from 'express'
import files from './data/data.js'
const router = express.Router()
router.use('/files', files)
export default router
