import getApp from './app.js'
import http from 'http'
const port = process.env.PORT
const app = await getApp().catch((err) => {
  throw err
})
const server = http.createServer({}, app)
try {
  server
    .listen(port, () => console.info(`server running on port : ${port}`))
    .on('error', (e) => console.error('server.listen error.', e))
} catch (err) {
  console.log(err)
}

export default server
