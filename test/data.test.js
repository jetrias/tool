import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../src/server.js'
// eslint-disable-next-line no-unused-vars
const should = chai.should()
chai.use(chaiHttp)

// eslint-disable-next-line no-undef
describe('testing endpoints', function () {
  // eslint-disable-next-line no-undef
  it('testing get data from files and response is an object', function (done) {
    chai
      .request(server)
      .get('/v1/files/data')
      // eslint-disable-next-line n/handle-callback-err
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        done()
      })
  })
  // eslint-disable-next-line no-undef
  it('testing get files and files contains an array', function (done) {
    chai
      .request(server)
      .get('/v1/files/list')
      // eslint-disable-next-line n/handle-callback-err
      .end((err, res) => {
        res.should.have.status(200)
        res.body.files.should.be.a('array')
        done()
      })
  })
})
