# Descripción
Este proyecto es un API REST que toma información de un API externa y la reformatea para exponerla.

El proyecto esta desarrollado con Nodejs, Expressjs
Las librerias utilizadas para las pruebas unitarias son Mocha + Chai

El API externo del cual se debe tomar la informacion lo puedes ver en el siguiente link <https://echo-serv.tbxnet.com/explorer/#/Secret>

# 1. Uso del proyecto
## Instalación
Una vez que hayas descargado el proyecto del repositorio te ubicas en la carpeta del proyecto y ejecutas: 
```
npm install
```
## Ejecución
Una vez se hayan instalado todas las dependencias puedes ejecutar el proyecto usando el siguiente comando
```
npm start
```

## Configuración
Por defecto esta configurado para correr en la siguiente direccion y puerto *http://localhost:4000/v1/files/*

Si deseas cambiar el puerto  en el que corre o el servidor del cual consume el API externo debes modificarlo en el archivo **.env** ubicado en la raiz del proyecto y modificar las variables **PORT** y **SERVERDIR** respectivamente
```.env
PORT=4000
SERVERDIR=https://echo-serv.tbxnet.com/
```

## Composisión
Consta de 2 endpoints:
- El primero permite obtener una lista de archivos .csv del servidor, luego busca 1 a 1 los datos de los archivos en el servidor, formatea los datos de la manera deseada para finalmente exponerlos. El endpoint en cuestion lo obtienes consultando esta ruta:
```
http://localhost:4000/v1/files/data
````
- El segundo permite obtener la lista de archivos del servidor y exponerla tal como se muestra del servidor. Puedes acceder a este endpoint usando esta ruta:
```
http://localhost:4000/v1/files/list
```

### **Nota:** estos endpoints devuelven los datos en formato json

### Ejecución de pruebas
Las pruebas unitarias las puedes ejecutar con el comando
```
npm test
```

La verificacion de codigo estandard la ves ejecutando el comando
```
npx standard
```

A continuación te presento el diagrama de secuencias utilizado para el desarrollo del proyecto. En este desarrollo se contempla la sección del API y su interaccion ente el Frontend y el API Externo.

![Diagrama de secuencia](/images/diagrama.png)

## Estructura del proyecto
![Diagrama de secuencia](/images/estructura.png)

En el directorio **src** consigues la carpeta **routes** que se explica mas adelante, el archivo **app.js** donde se define la configuración de la aplicación y el archivo **server.js** donde se define la configuración del servidor.

En el directorio **routes/v1** consigues la carpeta **data** que contiene el archivo **data.js** donde esta la definición de los endpoints. 

En este mismo directorio consiges el archivo **index.js** donde se define la ruta de acceso a los endpoints.

Luego consigues el directorio **test** que contiene el archivo **data.test.js** donde definen las pruebas unitarias.

En la raiz estan:
- El archivo **.env** donde estan las variables de entorno de la aplicación.
- El archivo **mocharc.json** en el cual esta definida a configuracion de la libreria Mocha para la ejecucion de las pruebas.
- El archivo package-lock.json donde se encuantran todas las dependencias especificadas.
- El archivo **package.json** donde se encuentra la información general del proyecto.

## Liberias usadas
- Nodejs
- Expressjs
- Cors
- Axios
- Dotenv
- Mocha
- Chai
- Chai-http
- Standard

#### El repositorio de este proyecto es: <https://gitlab.com/jetrias/tool.git>
#### El Frontend que puedes usar con este backend lo consigues en *GITLAB* <https://gitlab.com/jetrias/tool_front.git>